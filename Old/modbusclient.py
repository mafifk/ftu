#!/usr/bin/env python
'''
Pymodbus Synchrnonous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

It should be noted that the client can also be used with
the guard construct that is available in python 2.5 and up::

    with ModbusClient('127.0.0.1') as client:
        result = client.read_coils(1,10)
        print result
'''
#---------------------------------------------------------------------------#
# import the various server implementations
#---------------------------------------------------------------------------#
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.bit_read_message import ReadBitsRequestBase
from pymodbus.bit_read_message import ReadBitsResponseBase
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
#from pymodbus.client.sync import ModbusUdpClient as ModbusClient
#from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import time, random

#---------------------------------------------------------------------------#
# configure the client logging
#---------------------------------------------------------------------------#

def Test(op):
    #---------------------------------------------------------------------------#
    # choose the client you want
    #---------------------------------------------------------------------------#
    # make sure to start an implementation to hit against. For this
    # you can use an existing device, the reference implementation in the tools
    # directory, or start a pymodbus server.
    #---------------------------------------------------------------------------#
    client = ModbusClient('192.168.0.113', 5020)

    #---------------------------------------------------------------------------#
    # example requests
    #---------------------------------------------------------------------------#
    # simply call the methods that you would like to use. An example session
    # is displayed below along with some assert checks.
    #---------------------------------------------------------------------------#

    result = client.read_holding_registers(0x00,8, unit=0x00)
    t = result.registers
    print t
    # client.write_registers(0x00, [1,2,3,4])
    # print client.read_coils(0,count=8).bits
    #
    # # Decoding
    # decoder = BinaryPayloadDecoder.fromRegisters(result.registers, endian=Endian.Little)
    # decoded = {
    #     'string': decoder.decode_string(8),
    #     'float': decoder.decode_32bit_float(),
    #     '16uint': decoder.decode_16bit_uint(),
    #     '8int': decoder.decode_8bit_int(),
    #     'bits': decoder.decode_bits(),
    # }
    # print decoded
    # print decoded["string"]
    client.write_coils(0, [op, False, False, False, False, False, False, False])
    print client.read_coils(0,count=8).bits
    #client.write_coils(0, [False, False, False, False, False, False, False, False])
    #print client.read_coils(0,count=8).bits
    #---------------------------------------------------------------------------#
    # close the client
    #---------------------------------------------------------------------------#
    client.close()

while True:
    Test(True)
    time.sleep(random.randint(0,10))
    Test(False)
    time.sleep(random.randint(0,10))