from pymodbus.constants import Endian
from pymodbus.server.async import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSlaveContext, ModbusServerContext
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from twisted.internet.task import LoopingCall
import RPi.GPIO as GPIO
from threading import Thread
import threading
from time import sleep
import random
import os
import datetime

init_value = 0
event_trigger = [False,False]

def led_ouput(a,):
    context = a[0]
    register = 3
    slave_id = 0x00
    address = 0x00
    value = context[slave_id].getValues(1,0)[0]

    if value is False:
        #print "Disable"
        GPIO.output(4, 1) # Turn off
    else:
        #print "Enable"
        GPIO.output(4, 0)  # Turn On


def updating_writer(a):
    global init_value
    global event_trigger
    # print "updating_writer"
    context = a[0]
    register = 3
    slave_id = 0x00
    address = 0x00
    # global pwmDutyCycle,temp
    # #uncomment to debug temperature
    # print temp.getCurrentTemp()
    # values = [int(pwmDutyCycle),temp.getCurrentTemp()*100]
    if init_value <= 0 :
        #context[slave_id].setValues(register,address,[2])
        init_value = 2
        context[slave_id].setValues(1,address,[1,0,1,0,1])
    #print GPIO.input(17)
    if GPIO.input(17) >= 1:
        if event_trigger[0] is False:
            time_now=datetime.datetime.utcnow()
            new_format = [int(str(time_now.year)[:2]), int(str(time_now.year)[2:]), time_now.month, time_now.day, time_now.hour,
                          time_now.minute, time_now.second, int((str(int(time_now.microsecond/1000)).format(4))[:2]), int((str(int(time_now.microsecond/1000)).format(4))[2:])]
            print new_format
            context[slave_id].setValues(register,address, new_format)
            event_trigger = [True, False]
        context[slave_id].setValues(1,0x01,[1])
        #print "Enable"
    else:
        #print "Disable"
        if event_trigger[1] is False:
            time_now=datetime.datetime.utcnow()
            new_format = [int(str(time_now.year)[:2]), int(str(time_now.year)[2:]), time_now.month, time_now.day, time_now.hour,
                          time_now.minute, time_now.second, int((str(int(time_now.microsecond/1000)).format(4))[:2]), int((str(int(time_now.microsecond/1000)).format(4))[2:])]
            print new_format
            context[slave_id].setValues(register,address, new_format)
            event_trigger = [False, True]
        context[slave_id].setValues(1,0x01,[0])



def read_context(a):
    #print "Read Context!!!"
    context = a[0]
    register = 3
    slave_id = 0x00
    address = 0x00
    value = context[slave_id].getValues(register,address)
    #print value


def main():
    store = ModbusSlaveContext(
        di=ModbusSequentialDataBlock(1, [0]*100),
        co=ModbusSequentialDataBlock(1, [0]*100),
        hr=ModbusSequentialDataBlock(1, [0]*100),
        ir=ModbusSequentialDataBlock(1, [0]*100))
    context = ModbusServerContext(slaves=store, single=True)
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/simplyautomationized'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'
    identity.MajorMinorRevision = '1.0'
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(4, GPIO.OUT)
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    time = 5 # 5 seconds delaytime = 5 # 5 seconds delay
    writer = LoopingCall(read_context,a=(context,))
    loop = LoopingCall(updating_writer, a=(context,))
    led_out = LoopingCall(led_ouput, a=(context,), )
    loop.start(.1) # initially delay by time
    writer.start(.1)
    led_out.start(.1)
    StartTcpServer(context, identity=identity, address=("0.0.0.0", 5020))#, address=("localhost", 502))
    #cleanup async tasks
    #temp.setEnabled(False)
    #loop.stop()
    #writer.stop()
    #GPIO.cleanup()
if __name__ == "__main__":
    # temp = Temp()
    # temp.start()
    main()