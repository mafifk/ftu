import zmq.green as zmq
import gevent
import datetime
import cPickle
from gevent.lock import BoundedSemaphore
import time


class IOService(gevent.Greenlet):
    def __init__(self, ipc_location):
        gevent.Greenlet.__init__(self)
        self.context = zmq.Context.instance()
        self.socket = self.context.socket(zmq.ROUTER)  # Sharing information/update to the interested service
        self.socket.bind("ipc://"+str(ipc_location))
        self.thread = list()
        self.client_list = dict()
        self.data_template = {"Enable": False, "TimeStamp": None, "Status": None }
        self.digital_input = {00: self.data_template.copy(),
                              01: self.data_template.copy(),
                              02: self.data_template.copy(),
                              03: self.data_template.copy()}
        self.digital_output = {00: self.data_template.copy()}
        self.cmd = {"RefreshDigitalOutput": self.refresh_digital_output_status,
                    "RefreshDigitalInput": self.refresh_digital_input_status,
                    "WriteDigitalOutput": self.write_digital_output_status,
                    "EnableDigitalOutput": self.enable_digital_output,
                    "EnableDigitalInput": self.enable_digital_input,
                    "DisableDigitalInput": self.disable_digital_input,
                    "DisableDigitalOutput": self.disable_digital_output,
                    "Register": self.register_client
                    }
        self.cmd_fifo_exe_status_write_lock = BoundedSemaphore(1)
        self.cmd_fifo_exe_status = False
        self.receive_buffer = [] # {Operation, kwargs param}
        self.receive_buffer_write_lock = BoundedSemaphore(1)
        self.enable_client_id = ("TEST_Client")
        self.send_data_buffer = []
        self.buffer_dry_lock_write = BoundedSemaphore(1)
        self.send_data_buffer_lock_write = BoundedSemaphore(1)
        self.send_data_buffer = []
        self.buffer_dry = True

    def add_to_send_buffer(self, data):
        self.send_data_buffer_lock_write.acquire()
        client_list = data["ClientName"]
        del data["ClientName"]
        serial_data = cPickle.dumps(data, -1)
        data_to_client = {
            "ClientName": client_list,
            "SerialData": serial_data
                          }
        self.send_data_buffer.append(data_to_client)
        self.send_data_buffer_lock_write.release()
        if self.buffer_dry is True:
            self.buffer_dry_lock_write.acquire()
            self.buffer_dry = False
            self.buffer_dry_lock_write.release()
            self.thread.append(gevent.Greenlet.spawn(self.send_data_to_client))
        gevent.sleep(0)

    def send_data_to_client(self):  # Automatic based on fifo buffer
        self.buffer_dry_lock_write.acquire()
        while self.buffer_dry is False:
            buffer_data = self.send_data_buffer.pop(0)
            print buffer_data
            self.socket.send_multipart([buffer_data["ClientName"], buffer_data["SerialData"]])
            if len(self.send_data_buffer) == 0:
                break
        self.buffer_dry = True
        self.buffer_dry_lock_write.release()
        self.__exit_green()

    def cmd_exe(self):
        self.cmd_fifo_exe_status_write_lock.acquire()
        while self.cmd_fifo_exe_status is True:
            self.receive_buffer_write_lock.acquire()
            cmd_to_exe = self.receive_buffer.pop(0)
            self.receive_buffer_write_lock.release()
            if self.cmd.has_key(cmd_to_exe["Operation"]):
                param = dict.copy(cmd_to_exe)
                del param["Operation"]
                self.cmd[cmd_to_exe["Operation"]](**param)
            if len(self.receive_buffer) == 0:
                break
        self.cmd_fifo_exe_status = False
        self.cmd_fifo_exe_status_write_lock.release()
        self.__exit_green()

    def register_client(self, **kwargs):
        data = {"ClientName": kwargs["ClientName"],
                "Operation": "AuthRegister",
                "Status": True}
        self.add_to_send_buffer(data)

        # reply back to said it is register

    def refresh_digital_input_status(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_input.has_key(kwargs["PortID"]):
                return self.digital_input[kwargs["PortID"]]

    def refresh_digital_output_status(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_output.has_key(kwargs["PortID"]):
                return self.digital_output[kwargs["PortID"]]

    def enable_digital_input(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_input.has_key(kwargs["PortID"]):
                self.digital_input[kwargs["PortID"]]["Enable"] = True

    def disable_digital_input(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_input.has_key(kwargs["PortID"]):
                self.digital_input[kwargs["PortID"]] = self.data_template.copy()

    def enable_digital_output(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_input.has_key(kwargs["PortID"]):
                self.digital_output[kwargs["PortID"]] = True

    def disable_digital_output(self, **kwargs):
        if kwargs.has_key("PortID"):
            if self.digital_input.has_key(kwargs["PortID"]):
                self.digital_output[kwargs["PortID"]]["Enable"] = self.data_template.copy()

    def write_digital_output_status(self, **kwargs):
        if (kwargs.has_key("Status")) and (kwargs.has_key("PortID")):
            if self.digital_output.has_key(kwargs["PortID"]):
                if (kwargs["Status"] is True) and (self.digital_output is False):
                    self.digital_output[kwargs["PortID"]]["Status"] = True
                    print "Turn it On..."
                    # If the same, the port will not write it back
                elif (kwargs["Status"] is False) and (self.digital_output is True):
                    print "Turn it Off..."
                    self.digital_output[kwargs["PortID"]]["Status"] = False
                self.digital_output[kwargs["PortID"]]["TimeStamp"] = datetime.datetime.utcnow()

    def monitor_digital_input(self, digital_input_port):
        while True:
            #print "Keep Monitoring..."
            #print digital_input_port
            gevent.sleep(0.001)
            # if currentstate different, update
        print "Exiting..."

    def command_receiver(self):
        while True:
            try:
                client_id, data = self.socket.recv_multipart()
                data = cPickle.loads(data)
                if client_id in self.enable_client_id:
                    self.receive_buffer_write_lock.acquire()
                    data["ClientName"] = client_id  # Make secure com id list ???
                    self.receive_buffer.append(data)
                    self.receive_buffer_write_lock.release()
                    if self.cmd_fifo_exe_status is False:
                        self.cmd_fifo_exe_status_write_lock.acquire()
                        self.cmd_fifo_exe_status = True
                        self.cmd_fifo_exe_status_write_lock.release()
                        self.thread.append(gevent.Greenlet.spawn(self.cmd_exe))
            except:
                print "Shit!"


            #if self.cmd_fifo_exe_status is False:
            #    self.cmd_fifo_exe_status_write_lock.acquire()
            #    self.cmd_fifo_exe_status = True
            gevent.sleep(0)

    def __exit_green(self):
        self.thread.remove(gevent.getcurrent())

    def _run(self):
        for input_task in self.digital_input.keys():
            self.thread.append(gevent.Greenlet.spawn(self.monitor_digital_input, input_task))
            # print input_task
        #self.thread.append(gevent.Greenlet.spawn(self.send_data,))
        self.thread.append(gevent.Greenlet.spawn(self.command_receiver))
        # Broadcaster
        #gevent.Greenlet.spawn(self.cam_process)
        while len(self.thread) > 0:
            gevent.sleep(0.1)
        print "Exiting!"



if __name__ == '__main__':
    io_serveice = IOService("test")
    io_serveice.start()
    io_serveice.join()
    #recorder.enable_digital_input(PortID=00)
    #print recorder.digital_input
    #print "Test Bench!!!"

