import zmq.green as zmq
import gevent
import datetime
import cPickle
from gevent.lock import BoundedSemaphore


class IOServiceAPI(gevent.Greenlet):
    def __init__(self, ipc_location, id):
        gevent.Greenlet.__init__(self)
        self.id = id
        self.context = zmq.Context.instance()
        self.context.setsockopt(zmq.IDENTITY, str(id))
        self.socket = self.context.socket(zmq.DEALER)  # Sharing information/update to the interested service
        self.socket.connect("ipc://"+str(ipc_location))
        self.thread = list()
        self.cmd = {
            "AuthRegister": self.auth_register
                    }
        self.cmd_fifo_exe_status_write_lock = BoundedSemaphore(1)
        self.cmd_fifo_exe_status = False
        self.receive_buffer = [] # {Operation, kwargs param}
        self.receive_buffer_write_lock = BoundedSemaphore(1)
        self.enable_client_id = ("TEST_Client")
        self.send_data_buffer = []
        self.buffer_dry_lock_write = BoundedSemaphore(1)
        self.send_data_buffer_lock_write = BoundedSemaphore(1)
        self.send_data_buffer = []
        self.buffer_dry = True
        self.connect_io_service = False

    # Extended Functions start here
    # Extended functions end here

    def auth_register(self, **kwargs):
        if kwargs["Status"] is True:
            self.connect_io_service = True
            print "Ok"
        else:
            data = {"Operation": "Register"}
            self.add_to_send_buffer(data)

    def add_to_send_buffer(self, data):
        self.send_data_buffer_lock_write.acquire()
        serial_data = cPickle.dumps(data, -1)
        data_to_client = {
            "SerialData": serial_data
                          }
        self.send_data_buffer.append(data_to_client)
        self.send_data_buffer_lock_write.release()
        if self.buffer_dry is True:
            self.buffer_dry_lock_write.acquire()
            self.buffer_dry = False
            self.buffer_dry_lock_write.release()
            self.thread.append(gevent.Greenlet.spawn(self.send_data_to_client))
        gevent.sleep(0)

    def send_data_to_client(self):  # Automatic based on fifo buffer
        self.buffer_dry_lock_write.acquire()
        while self.buffer_dry is False:
            buffer_data = self.send_data_buffer.pop(0)
            self.socket.send(buffer_data["SerialData"])
            if len(self.send_data_buffer) == 0:
                break
        self.buffer_dry = True
        self.buffer_dry_lock_write.release()
        self.__exit_green()

    def cmd_exe(self):
        self.cmd_fifo_exe_status_write_lock.acquire()
        while self.cmd_fifo_exe_status is True:
            self.receive_buffer_write_lock.acquire()
            cmd_to_exe = self.receive_buffer.pop(0)
            self.receive_buffer_write_lock.release()
            if self.cmd.has_key(cmd_to_exe["Operation"]):
                param = dict.copy(cmd_to_exe)
                del param["Operation"]
                self.cmd[cmd_to_exe["Operation"]](**param)
            if len(self.receive_buffer) == 0:
                break
        self.cmd_fifo_exe_status = False
        self.cmd_fifo_exe_status_write_lock.release()
        self.__exit_green()

    def register_to_io(self):
        data = {"Operation": "Register"}
        self.add_to_send_buffer(data)
        self.__exit_green()

    def command_receiver(self):
        while True:
            try:
                data = self.socket.recv_multipart()[0]
                data = cPickle.loads(data)
                self.receive_buffer_write_lock.acquire()
                self.receive_buffer.append(data)
                self.receive_buffer_write_lock.release()
                if self.cmd_fifo_exe_status is False:
                    self.cmd_fifo_exe_status_write_lock.acquire()
                    self.cmd_fifo_exe_status = True
                    self.cmd_fifo_exe_status_write_lock.release()
                    self.thread.append(gevent.Greenlet.spawn(self.cmd_exe))
            except:
                print "Shit!"

    def __exit_green(self):
        self.thread.remove(gevent.getcurrent())

    def _run(self):
        self.thread.append(gevent.Greenlet.spawn(self.command_receiver))
        self.thread.append(gevent.Greenlet.spawn(self.register_to_io))
        while len(self.thread) > 0:
            gevent.sleep(0.1)
        print "Exiting!"


if __name__ == '__main__':
    io_serveice = IOServiceAPI("test", "TEST_Client")
    io_serveice.start()
    io_serveice.join()
    #recorder.enable_digital_input(PortID=00)
    #print recorder.digital_input
    #print "Test Bench!!!"

